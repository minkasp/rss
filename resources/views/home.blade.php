@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row" id="feed-row">
        <div class="jumbotron">
            <h1>{{trans('Top 10 RSS feeds')}}</h1>
            <p>
                {{
                    trans('Here you can find all relevant news in one place.')
                }}
            </p>
            @if(!empty($feeds))
                @if(!empty($categories))
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-8" style="padding-right: 0">
                                            <input class="cat" type="hidden" name="category" value="-1"/>
                                            <select class="form-control category-filter-select">
                                                <option value="-1">{{trans('Select Category')}}</option>
                                                @foreach($categories['_items'] as $category)
                                                    <option value="{{$category['name']}}">{{$category['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endif
                {{--feeds table--}}
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>{{trans('Feed Provider')}}</th>
                        <th>{{trans('Title')}}</th>
                        <th>{{trans('Category')}}</th>
                        <th>{{trans('Subcategory')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($feeds as $feed)
                            <tr data-link="{{$feed->link}}" data-description="{{$feed->description}}" data-title="{{$feed->title}}">
                                <th><a href="{{$feed->owner_page_url}}" target="_blank">{{$feed->owner_title or trans("Unknown")}}</a></th></th>
                                <td><img src="{{$feed->owner_logo_url}}" style="height: 15px; margin-right: 10px;"/><a class="feed-link" href="{{$feed['link']}}">{{$feed['title']}}</a></td>
                                <td>{{(isset($feed->provider->category) ? $feed->provider->category->name : trans('Not Set'))}}</td>
                                <td>{{$feed->category ?: trans('Other') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning">
                    <strong>{{trans('Notice')}}</strong> {{trans('At this moment we can not fetch any news feeds, please come back later.')}}
                </div>
            @endif
        </div>

        {!! $feeds !!}
       <?php
        //echo $feeds->render();
       ?>
        {{--each feed modal info--}}
        <div class="modal fade feed-modal"  tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal-title"></h4>
                    </div>
                    <div class="modal-body" id="modal-description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('Close')}}</button>
                        <a id="modal-link" href="#" type="button" class="btn btn-primary">{{trans('Go to Feed Page')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').on('click', '.category-filter-select', function(){
                var cat = $(this).val();
                if (cat != -1) {
                    var url = "{{Request::url()}}";
                    url += "?category="+encodeURIComponent(cat);
                    $.ajax({
                        method: "get",
                        url: url,
                    })
                    .success(function(content) {
                        var content = $(content).find('#feed-row');
                        $('#feed-row').empty();
                        $('#feed-row').html(content);
                    });
                }
            });

            $('body').on('click', '.feed-link', function(e){
                e.preventDefault();

                var title = $(this).parent().parent().data('title');
                var link = $(this).parent().parent().data('link');
                var desc = $(this).parent().parent().data('description');

                var modal = $('.feed-modal');

                $('#modal-title').html(title);
                $('#modal-link').attr('href', link);
                $('#modal-description').html(desc);


                modal.modal('show');
            });
        });
    </script>
</div>
@endsection
