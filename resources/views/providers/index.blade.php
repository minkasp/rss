@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="jumbotron">
            <h1>{{trans('RSS feed Providers')}}</h1>
            @if(Session::has('success') && session('success') == true)
                <div class="alert alert-success">
                    <strong>{{trans('Success!')}}</strong> {{trans('Your changes have been saved successfully :)')}}
                </div>
            @elseif(Session::has('success') && session('success') == false)
                <div class="alert alert-danger">
                    <strong>{{trans('Failure!')}}</strong> {{trans('We couldnt\'t save your changes :(')}}
                </div>
            @endif
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('Provider Source Link')}}</th>
                        <th>{{trans('Category')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/providers') }}">
                            {{ csrf_field() }}
                            <td><input type="text" class="form-control" name="source" placeholder="{{trans('Provider Source')}}" autocomplete="off"></td>
                            <td>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input class="cat" type="hidden" name="category" value="-1"/>
                                        <select class="form-control">
                                            <option value="-1">{{trans("Select Category")}}</option>
                                            @foreach($categories['_items'] as $category)
                                                <option value="{{$category['id']}}">{{$category['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td><button type="submit" class="btn btn-primary col-xs-10">{{trans('Add')}}</button></td>
                        </form>
                    </tr>
                    @foreach ($providers['_items'] as $index => $provider)
                        <tr>
                            <form class="form-horizontal form" role="form" method="POST" action="{{ url('/providers/'.$provider['id']) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                 <td><input disabled type="text" class="form-control" name="source" value="{{ $provider['source'] }}"></td>
                                 @if (!empty($categories))
                                     <td>
                                         <div class="form-group">
                                             <div class="col-sm-12">
                                                 <input class="cat" type="hidden" name="category" value="-1"/>
                                                 <select disabled class="form-control category-select">
                                                     <option value="-1">{{trans("Select Category")}}</option>
                                                      @foreach($categories['_items'] as $category)
                                                           <option value="{{$category['id']}}" {{isset($provider['category']) && $provider['category'] == $category['name'] ? 'selected' : ''}}>{{$category['name']}}</option>
                                                       @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                     </td>
                                @endif
                                 <td><input type="button" class="btn btn-info edit-btn col-xs-10" value="{{trans('Edit')}}"/></td>
                            </form>
                            <td>
                                <form class="form-horizontal form-destroy" role="form" method="POST" action="{{ url('/providers/'.$provider['id']) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="button" class="close" aria-label="Remove"><span aria-hidden="true">×</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.edit-btn', function(){
            if ($(this).attr('value') == "{{trans("Edit")}}") {
                $(this).attr('value', "{{trans("Save")}}");
                console.log();
                $(this).parent().parent().find(':input[type=text]').each(function(index, el){
                    $(el).removeAttr('disabled');
                });
                $(this).parent().parent().find('select').each(function(index, el){
                    $(el).removeAttr('disabled');
                })
            } else {
                $(this).parent().parent().find('.form').submit();
            }
        });

        $('body').on('click', '.close', function(){
            $(this).parent().parent().find('.form-destroy').submit();
        });

        $('body').on('click', '.category-select', function(){
            var select = $(this).parent().parent().find(':input[type=hidden]');
            var id = $(this).val();

            select.click();
            select.attr('value', id);
        })
    });
</script>
@endsection
