@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="jumbotron">
            <h1>{{trans('RSS Feed Category Listing')}}</h1>
            @if(Session::has('success') && session('success') == true)
                <div class="alert alert-success">
                    <strong>{{trans('Success!')}}</strong> {{trans('Your changes have been saved successfully :)')}}
                </div>
            @elseif(Session::has('success') && session('success') == false)
                <div class="alert alert-danger">
                    <strong>{{trans('Failure!')}}</strong> {{trans('We couldnt\'t save your changes :(')}}
                </div>
            @endif
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('Category Name')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/categories') }}">
                            {{ csrf_field() }}
                            <td><input type="text" class="form-control" name="name" placeholder="{{trans('Category Name')}}" autocomplete="off"></td>
                            <td><button type="submit" class="btn btn-primary col-xs-10">{{trans('Add')}}</button></td>
                        </form>
                    </tr>
                    @foreach ($categories['_items'] as $index => $category)
                        <tr>
                            <form class="form-horizontal form" role="form" method="POST" action="{{ url('/categories/'.$category['id']) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                 <td><input disabled type="text" class="form-control" name="name" value="{{ $category['name'] }}"></td>
                                 <td><input type="button" class="btn btn-info edit-btn col-xs-10" value="{{trans('Edit')}}"/></td>
                            </form>
                            <td>
                                <form class="form-horizontal form-destroy" role="form" method="POST" action="{{ url('/categories/'.$category['id']) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="button" class="close" aria-label="Remove"><span aria-hidden="true">×</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.edit-btn', function(){
            if ($(this).attr('value') == "{{trans("Edit")}}") {
                $(this).attr('value', "{{trans("Save")}}");
                console.log();
                $(this).parent().parent().find(':input[type=text]').each(function(index, el){
                    $(el).removeAttr('disabled');
                })
            } else {
                $(this).parent().parent().find('.form').submit();
            }
        });

        $('body').on('click', '.close', function(){
            $(this).parent().parent().find('.form-destroy').submit();
        });
    });
</script>
@endsection
