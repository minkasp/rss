<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'status' => \App\User::USER_STATUS_ACTIVE
    ];
});

$factory->define(App\Feed::class, function (Faker\Generator $faker) {
    return [
        'provider_id' => 1,
        'owner_title' => $faker->text(20),
        'owner_page_url' => $faker->url,
        'title' => $faker->text(30),
        'category' => null,
        'publish_date' => \Carbon\Carbon::now(),
        'description' => $faker->realText(),
        'link' => $faker->url
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(45),
        'description' => $faker->realText(),
    ];
});

$factory->define(App\Provider::class, function (Faker\Generator $faker) {
    return [
        'url' => $faker->url,
        'category_id' => null,
    ];
});