![X3qSMiy.png](https://bitbucket.org/repo/kodLrx/images/3542509810-X3qSMiy.png)


## Environment Setup

1. Clone repo via git bash using command *git clone https://minkasp@bitbucket.org/minkasp/rss.git*
2. Create database and its managing user.
4. cd to project dir and run *composer install* to install necessary dependencies.
5. Run following commands (at project root dir) to setup your .env file *cp .env.example .env* and *php artisan key:generate* .
6. Create *migrations* table by runnning *php artisan migrate:install* .
and *php artisan migrate* respectively.
7. Run PHP server by running command *php artisan serve* - web page should be accessed at [http://127.0.0.1:8000/](http://127.0.0.1:8000/).
8.  Create admin user by running *php artisan user:create myemail@gmail.com mypassword*. NOTE: be sure to run this command in separate git bash window or stop your PHP server (CTRL+C).
9. (run your server again, if you had to stop it), login with the same credentials you've entered creating your user.
10. Navigate to **RSS Providers -> Providers** and add your first RSS Feed provider (add only RSS XML feed urls).
11. Navigate to **RSS Providers -> Categories** and add your RSS Feed category (any custom category you desire).
12. Navigate back to Navigate to **RSS Providers -> Providers** and assign your created category to the provider.
13. Fetch provider feeds by running *php artisan update:feed*
14. Navigate to the root page (index) to see your fetched providers.


## Filtering

You can filter your feeds in the home page as of now only by main category which can be set by admin user alone. Subcategory is a sensitive inner feed data that not all feed providers deliver through rss xml request, thus we do not have ability to set subcategory to all feeds. Filtering is done via ajax and the contents of the table are replaced by javascript all behind the scenes.

## Providers

1. http://www.15min.lt/rss
2. http://www.lrt.lt/naujienos?rss
3. http://www.delfi.lt/rss/feeds/daily.xml
4. http://www.delfi.lt/rss/feeds/politics.xml


## License

This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Testing

To run project unit tests `cd` to project root and run *phpunit*