<?php

namespace App\Transformers;

/**
 * Transforms array of transformer data into collection
 *
 * Class CollectionTransformer
 * @package App\Transformers
 */
class CollectionTransformer extends BaseTransformer
{
    /** @var Object  */
    protected $transformerInstance = null;

    /** @var array array of transformable data  */
    protected $array;

    /**
     * CollectionTransformer constructor.
     * @param $transformer
     * @param array $array
     */
    public function __construct($transformer, $array)
    {
        $this->transformerInstance = $transformer;
        $this->array = $array;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function transform()
    {
        $result['_items'] = [];

        if ($this->transformerInstance instanceof BaseTransformer) {
            foreach ($this->array as $data) {
                $result['_items'][] = $this->transformerInstance->transform($data);
            }

            return $result;
        }


        throw new \Exception(get_class($this->transformerInstance).' must be an instance of BaseTransformer');
    }
}