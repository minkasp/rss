<?php

namespace App\Transformers;

use App\User;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends BaseTransformer
{
    /**
     * Transforms User entity to array
     * @param Feed $feed|null
     * @return array
     */
    public function transform($user = null)
    {
        $result = [];

        if ($user != null) {
            $result = [
                'firstName' =>  $user->first_name,
                'lastName' => $user->last_name,
                'dateOfBorth' => $user->date_of_birth,
                'email' => $user->email,
                'username' => $user->username,
            ];
        }

        return $result;

    }

    /**
     * @param array $data
     * @param User $user
     * @return array
     */
    public function reverseTransform(array $data, User $user)
    {
        $this->setFieldIfExists($data, 'username', 'username', $user);
        $this->setFieldIfExists($data, 'password', 'password', $user, function($data, $user){
            $user->password = bcrypt($data['password']);
        });
        $this->setFieldIfExists($data, 'dateOfBirth', 'date_of_birth', $user);
        $this->setFieldIfExists($data, 'email', 'email', $user);
        $this->setFieldIfExists($data, 'firstName', 'first_name', $user);
        $this->setFieldIfExists($data, 'lastName', 'last_name', $user);
        $user->save();

        return $this->transform($user);
    }
}