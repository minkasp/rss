<?php

namespace App\Transformers;

use App\Provider;

/**
 * Class ProviderTransformer
 * @package App\Transformers
 */
class ProviderTransformer extends BaseTransformer
{
    /**
     * @param null $provider
     * @return array
     */
    public function transform($provider = null)
    {
        $result = [];

        if ($provider != null) {
            $result = [
                'id' => $provider->id,
                'source' =>  $provider->url,
                'category' => isset($provider->category) ? $provider->category->name : null,
            ];
        }

        return $result;

    }

    /**
     * @param array $data
     * @param Provider $provider
     * @return array
     */
    public function reverseTransform(array $data, Provider $provider)
    {
        $this->setFieldIfExists($data, 'source', 'url', $provider);
        $this->setFieldIfExists($data, 'category', 'category_id', $provider);

        $provider->save();

        return $this->transform($provider);
    }
}