<?php

namespace App\Transformers;

use App\Feed;

/**
 * Class FeedTransformer
 * @package App\Transformers
 */
class FeedTransformer extends BaseTransformer
{
    /**
     * Transforms Feed entity to array
     * @param Feed $feed|null
     * @return array
     */
    public function transform($feed = null)
    {
        $result = [];

        if ($feed != null) {
            $category = 'Unknown';

            if ($feed->category != null) {
               $category = $feed->category;
            }
            elseif ($feed->cat != null) {
                $category = __('Other');
            }

            $result = [
                'ownerLink' =>  $feed->owner_page_url,
                'ownerTitle' => $feed->owner_title,
                'title' => $feed->title,
                'ownerLogo' => $feed->owner_logo_url,
                'description' => $feed->description,
                'category' => $category,
                'publishDate' => $feed->publish_date,
                'link' => $feed->link,
            ];
        }

        return $result;

    }

    /**
     * @param array $data
     * @param Feed $feed
     */
    public function reverseTransform(array $data, Feed $feed)
    {
        $this->setFieldIfExists($data['title'], 'title', $feed);
        $this->setFieldIfExists($data['url'], 'owner_url', $feed);
        $this->setFieldIfExists($data['logo'], 'owner_logo_url', $feed);

        $feed->save();
    }
}