<?php
/**
 * Created by PhpStorm.
 * User: Mindaugas
 * Date: 1/21/2017
 * Time: 4:11 PM
 */

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

class BaseTransformer
{
    /**
     * @param $data
     * @param $key
     * @param $field
     * @param Model $entity
     * @param callable|null $callback
     */
    protected function setFieldIfExists($data, $key, $field, Model $entity, Callable $callback = null)
    {
        if (isset($data[$key])) {
            $entity->{$field} = $data[$key];
        }

        if ($callback != null) {
            $callback($data, $entity);
        }
    }
}