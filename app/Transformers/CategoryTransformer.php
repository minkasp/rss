<?php

namespace App\Transformers;

use App\Category;

/**
 * Class CategoryTransformer
 * @package App\Transformers
 */
class CategoryTransformer extends BaseTransformer
{
    /**
     * @param null $category
     * @return array
     */
    public function transform($category = null)
    {
        $result = [];

        if ($category != null) {
            $result = [
                'id' => $category->id,
                'name' =>  $category->name,
            ];
        }

        return $result;

    }

    /**
     * @param array $data
     * @param Category $category
     * @return array
     */
    public function reverseTransform(array $data, $category)
    {
        $this->setFieldIfExists($data, 'name', 'name', $category);

        $category->save();

        return $this->transform($category);
    }
}