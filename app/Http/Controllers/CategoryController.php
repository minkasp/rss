<?php

namespace App\Http\Controllers;

use App\Category;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Transformers\CollectionTransformer;
use App\Transformers\ProviderTransformer;
use App\Repositories\ProviderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    /** @var ProviderRepository|null  */
    protected $categoryRepository = null;

    /** @var ProviderTransformer|null  */
    protected $categoryTransformer = null;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $categoryRepository
     * @param CategoryTransformer $categoryTransformer
     */
    public function __construct(CategoryRepository $categoryRepository, CategoryTransformer $categoryTransformer)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryTransformer = $categoryTransformer;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $categories = $this->categoryRepository->getAllCategories();
        $response = (new CollectionTransformer($this->categoryTransformer, $categories))->transform();

        return view('categories.index', ['categories' => $response]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $category = $this->categoryRepository->findById($id);

        if ($category != null) {
            $this->categoryTransformer->reverseTransform($request->all(), $category);
        }

        return Redirect::back()->withSuccess(true);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->categoryTransformer->reverseTransform($request->all(), new Category());
        return Redirect::back()->withSuccess(true);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request, $id)
    {
        $category = $this->categoryRepository->findById($id);

        if ($category != null) {
            $category->delete();
        }

        return Redirect::back()->withSuccess(true);
    }
}
