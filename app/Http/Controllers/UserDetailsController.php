<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserDetailsController extends Controller
{
    /** @var UserRepository|null  */
    protected $userRepository = null;

    /** @var UserTransformer|null  */
    protected $transformer = null;

    /**
     * UserDetailsController constructor.
     * @param UserRepository $userRepo
     * @param UserTransformer $transformer
     */
    public function __construct(UserRepository $userRepo, UserTransformer $transformer)
    {
        $this->userRepository = $userRepo;
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id) {
            abort(404);
        }
        $user = $this->userRepository->findById($id);

        $validation = Validator::make($request->all(),  [
            'email' => 'required|email|unique:users,email,'. Auth::user()->id,
            'password' => 'required|min:6|confirmed',
            'firstName' => 'max:100',
            'lastName' => 'max:100',
            'username' => 'required|unique:users,username,'. Auth::user()->id,
        ]);

        if ($validation -> passes()) {
            $user = $this->transformer->reverseTransform($request->all(), $user);
            $request->session()->flash('status', true);

        } else {
            $request->session()->flash('status', false);
            $user = $this->transformer->transform($user);
        }

        return view('details.index', ['user' => $user])->withErrors($validation->errors());
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        if ($id != Auth::user()->id) {
            abort(404);
        }

        $user = $this->userRepository->findById($id);

        return view('details.index', ['user' => $this->transformer->transform($user)]);
    }

}
