<?php

namespace App\Http\Controllers;

use App\Provider;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Transformers\CollectionTransformer;
use App\Transformers\ProviderTransformer;
use App\Repositories\ProviderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RssProviderController extends Controller
{
    /** @var ProviderRepository|null  */
    protected $providerRepository = null;

    /** @var ProviderTransformer|null  */
    protected $providerTransformer = null;

    /** @var CategoryRepository|null  */
    protected $categoryRepository = null;

    /**
     * RssProviderController constructor.
     * @param ProviderRepository $providerRepository
     * @param ProviderTransformer $providerTransformer
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        ProviderRepository $providerRepository,
        ProviderTransformer $providerTransformer,
        CategoryRepository $categoryRepository
    )
    {
        $this->providerRepository = $providerRepository;
        $this->providerTransformer = $providerTransformer;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $providers = $this->providerRepository->getAllProviders();
        $categories = (new CollectionTransformer(new CategoryTransformer(), $this->categoryRepository->getAllCategories()))->transform();
        $response = (new CollectionTransformer($this->providerTransformer, $providers))->transform();

        return view('providers.index', ['providers' => $response, 'categories' => $categories]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $provider = $this->providerRepository->findById($id);

        if ($provider != null) {
            $this->providerTransformer->reverseTransform($request->all(), $provider);
        }

        return Redirect::back()->withSuccess(true);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->providerTransformer->reverseTransform($request->all(), new Provider());
        return Redirect::back()->withSuccess(true);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request, $id)
    {
        $provider = $this->providerRepository->findById($id);

        if ($provider != null) {
            $provider->delete();
        }

        return Redirect::back()->withSuccess(true);
    }

}
