<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\FeedRepository;
use App\Transformers\CollectionTransformer;
use App\Transformers\FeedTransformer;
use Illuminate\Http\Request;
use App\Transformers\CategoryTransformer;
use App\Filters\FeedFilter;

class HomeController extends Controller
{
    /** @var null  */
    protected $feedRepository = null;

    /** @var CategoryRepository|null  */
    protected $categoryRepository = null;

    /**
     * HomeController constructor.
     * @param FeedRepository $feedRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(FeedRepository $feedRepository, CategoryRepository $categoryRepository)
    {
        $this->feedRepository = $feedRepository;
        $this->categoryRepository = $categoryRepository;

       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = (new CollectionTransformer(new CategoryTransformer(), $this->categoryRepository->getAllCategories()))->transform();
        $feeds = (new FeedFilter($request->all(), $this->feedRepository->qbGetAllFeeds()))->filter();
//        $response = (new CollectionTransformer(new FeedTransformer(), $feeds))->transform();

        return view('home', ['feeds' => $feeds,  'categories' => $categories]);
    }
}
