<?php

namespace App\Filters;
use Illuminate\Database\Eloquent\Builder;

class BaseFilter implements FilterInterface
{
    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var Builder|null
     */
    protected $qb = null;

    /**
     * FeedFilter constructor.
     * @param array $params
     * @param Builder $qb
     */
    public function __construct(array $params = [], Builder $qb)
    {
        $this->params = $params;
        $this->qb = $qb;
    }

    public function filter(){}
}