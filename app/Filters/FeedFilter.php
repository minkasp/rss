<?php

namespace App\Filters;

use App\Repositories\FeedRepository;
use Illuminate\Database\Eloquent\Builder;

class FeedFilter extends BaseFilter
{
    /** defines how many entries to show per page */
    const PAGINATION_STEP = 30;

    /** @var \Illuminate\Foundation\Application|mixed|null  */
    protected $feedRepository = null;

    /**
     * FeedFilter constructor.
     * @param array $params
     * @param Builder $qb
     */
    public function __construct(array $params = [], Builder $qb)
    {
        $this->feedRepository = app(FeedRepository::class);
        parent::__construct($params, $qb);
    }

    /**
     * @return Builder|null
     */
    public function filter()
    {
        if (!empty($this->params)) {
            if (isset($this->params['category'])) {
                $this->qb = $this->feedRepository->filterByProviderCategory($this->params['category'], $this->qb);
            }
        }

        return $this->qb->paginate(self::PAGINATION_STEP);
    }
}