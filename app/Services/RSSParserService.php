<?php

namespace App\Services;

use Illuminate\Contracts\Logging\Log;
use SimpleXMLElement;

class RSSParserService
{
    /** @var null|string defines rss feed source */
    protected $sources = [];

    /** @var Feed object  */
    protected $feeds = [];

    /**
     * RSSParser constructor.
     * @param array $sources
     */
    public function __construct($sources = [])
    {
        $this->sources = $sources;
    }

    /**
     * @param array $sources
     * @return $this
     */
    public function setSources($sources)
    {
        if (!is_array($sources)) {
            $sources = array($sources);
        }

        $this->sources = $sources;

        return $this;
    }

    /**
     * Sets parsed feed data and returns it, if parsed with no errors
     *
     * @return Feed
     */
    public function parse()
    {
        try {
            if (!empty($this->sources)) {
                $this->feeds = $this->retrieveFeed();
            }

        } catch (\Exception $e) {
            Log::error("[".self::class."::parse() ] ". $e->getMessage());

            return null;
        }

        return $this->feeds;
    }

    /**
     * Retrieves possible feed data from specified source
     * @return Feed
     */
    private function retrieveFeed()
    {
        $feeds = [];

        foreach ($this->sources as $source) {
            $content = file_get_contents($source);
            $feeds[] = new FeedService(new SimpleXmlElement($content));
        }

        return $feeds;
    }

    /**
     * @return Feed
     */
    public function getFeeds()
    {
        return $this->feeds;
    }

}