<?php

namespace App\Services;

/**
 * Class FeedService
 * @package App\Services
 */
class FeedService
{
    /** @var null|\SimpleXMLElement feed xml object */
    protected $element = null;

    /** @var null|string feed self link string  */
    protected $link = null;

    /** @var null|string feed language string  */
    protected $lang = null;

    /** @var array feed data array */
    protected $items = [];

    /** @var null|string feed image url string  */
    protected $img = null;

    /** @var null| string feed owner title */
    protected $title = null;

    /**
     * Feed constructor.
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        $this->element = $element;
        $this->init();
    }

    /**
     * Function loads all the feed data and populates memory
     */
    private function init()
    {
        // filling news data array
        $this->fill($this->element);

        $this->title = isset($this->element->channel->title) ? (string) $this->element->channel->title : null;
        $this->lang = isset($this->element->channel->language) ? (string) $this->element->channel->language : null;
        $this->link = isset($this->element->channel->link)  ? (string) $this->element->channel->link : null;
        $this->img = isset($this->element->channel->image) ? (string) $this->element->channel->image->url : null;
    }

    /**
     * @param \SimpleXMLElement $e
     */
    protected function fill(\SimpleXMLElement $e)
    {
        $data = [];

        foreach ($e->channel->item as $item) {
            $data[] = [
                'title' => (string) $item->title,
                'link' => (string) $item->link,
                'publish_date' => (string) $item->pubDate,
                'comments_link' => (string) $item->comments,
                'description' => (string) $item->description,
                'category' => (string) $item->category
            ];
        }

        $this->items = $data;
    }

    /**
     * $this->items getter
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * $this->link getter
     * @return null|string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * $this->lang getter
     * @return null|string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * $this->img getter
     * @return null|string
     */
    public function getImageUrl()
    {
        return $this->img;
    }

    /**
     * $this->title getter
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }
}