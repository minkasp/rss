<?php

namespace App\Repositories;

use App\Provider;

class ProviderRepository extends Repository implements RepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Provider::find($id);
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getByCategory($category)
    {
        return Provider::where('category', $category)->get();
    }

    /**
     * @param $source
     * @return mixed
     */
    public function findBySource($source)
    {
        return Provider::where('url', '=', $source)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllProviders()
    {
        return Provider::all();
    }
}