<?php

namespace App\Repositories;

use App\Feed;
use Illuminate\Database\Eloquent\Builder;

class FeedRepository extends Repository implements RepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Feed::find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function qbGetAllFeeds()
    {
        return (new Feed())->newQuery();
    }

    /**
     * @param $categoryName
     * @param Builder|null $qb
     * @return Builder|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function filterByProviderCategory($categoryName, Builder $qb = null)
    {
        if ($qb == null) {
            $qb = $this->qbGetAllFeeds();
        }

        $qb = $qb->joinProviderCategoriesWithName($categoryName);

        return $qb;
    }
}