<?php

namespace App\Repositories;

use App\Category;

class CategoryRepository extends Repository implements RepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Category::find($id);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function findByName($name)
    {
        return Category::where('name',$name)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCategories()
    {
        return Category::all();
    }
}