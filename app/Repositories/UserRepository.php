<?php

namespace App\Repositories;

use App\User;

class UserRepository extends Repository implements RepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return User::find($id);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}