<?php

namespace App\Console\Commands;

use App\Feed;
use App\Repositories\ProviderRepository;
use App\Services\RSSParserService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates Feeds table with more goodies';

    /** @var RSSParser|null  */
    protected $parser = null;

    /** @var ProviderRepository|null  */
    protected $providerRepo = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RSSParserService $parser, ProviderRepository $providerRepo)
    {
        $this->providerRepo = $providerRepo;
        $this->parser = $parser;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers = $this->providerRepo->getAllProviders();
        $sources = $providers->pluck('url')->toArray();
        $feeds = $this->parser->setSources($sources)->parse();

        foreach ($feeds as $index => $feed) {
            $this->info("Iterating through retrieved Feeds: [{$feed->getLink()}] [{$feed->getTitle()}]");
            $provider = $providers[$index];
            $parentData = [
                'provider_id' => $provider->id,
                'owner_title' => $feed->getTitle(),
                'owner_page_url' => $feed->getLink(),
                'owner_logo_url' => $feed->getImageUrl(),
            ];

            foreach ($feed->getItems() as $feedData) {

                try {
                    $date = Carbon::parse($feedData['publish_date']);
                } catch (\Exception $e) {
                    $date = Carbon::now();
                }

                $data = [
                    'title' => $feedData['title'],
                    'category' => $feedData['category'],
                    'publish_date' => $date,
                    'description' => $feedData['description'],
                    'link' => $feedData['link'],
                ];

                $merged = array_merge($data, $parentData);
                Feed::updateOrCreate(['link' => $feedData['link']], $merged);

                $this->info("Saving feed: [{$feedData['link']}]");
            }
        }

        $this->info('Done.');
    }
}
