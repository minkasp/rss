<?php

namespace App\Console\Commands;

use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use App\User;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Created user by specified email address and password. E.g php artisan user:create google@something my-secret-password';

    /** @var UserRepository|null  */
    protected $userRepository = null;

    /**
     * CreateUser constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $email = $this->argument('email');
            $password = $this->argument('password');
            $exists = $this->userRepository->findByEmail($email) != null;
            $context = ($exists ? "Updating" : "Creating");

            $this->warn("User with such email [$email] already exists!");
            $this->info($context." user with Email: {$email} , Password: {$password}");
            $answer = $this->ask("Proceed? Y/n", 'y');

            if (strtolower($answer) === 'y') {
                $status = User::updateOrCreate(['email' => $email], [
                    'email' => $email,
                    'password' => bcrypt($password),
                    'status' => User::USER_STATUS_ACTIVE,
                    'username' => str_random('16')
                ]);

                $this->info('User '. ($status == true ? $context.' successfully' : 'couldn\'t be '.$context));
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

    }
}
