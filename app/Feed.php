<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'provider_id', 'owner_title', 'owner_page_url', 'owner_logo_url', 'title',
        'category', 'publish_date', 'description', 'link'
    ];

    /**
     * @param $query
     * @param $categoryName
     * @return mixed
     */
    public function scopeJoinProviderCategoriesWithName($query, $categoryName)
    {
        // to avoid future table join collision
        $append = str_random(16);

        $query->join('providers as '.$append.'_providers', function($join) use($append){
            $join->on('feeds.provider_id', '=', $append.'_providers.id');
        })->join('categories as '.$append.'_categories', function($join) use($append, $categoryName){
            $join->on($append.'_providers.category_id', '=', $append.'_categories.id')
                ->where($append.'_categories.name', '=', $categoryName);
        })->whereRaw($append.'_categories.id IS NOT NULL')
        ->selectRaw('feeds.*');

        return $query;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * Main category set by user through pivot provider table
     * @return mixed
     */
    public function cat()
    {
        return $this->provider->category;
    }

}
