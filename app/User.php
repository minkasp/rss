<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /** defines fully functional user account */
    const USER_STATUS_ACTIVE = 1;

    /** defines user who registered, but not verified his/her account yet */
    const USER_STATUS_NOT_VERIFIED = 0;

    /** defines user whose account has been disabled */
    const USER_STATUS_DISABLED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
