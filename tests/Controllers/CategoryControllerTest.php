<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class CategoryControllerTest extends \TestCase
{

    protected $categories = [];

    /**
     * Sets up needed data for testing
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Random category generator function
     * @return array
     */
    private function generateRandomCategories()
    {
        $count = random_int(5, 25);
        $categories[] = factory(App\Category::class, $count)->create();

        return $categories;
    }

    /**
     * Creates user actor instance for authenticated user
     */
    private function createAdminUser()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)->visit('/');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCategoryIndexAuthenticated()
    {
        $this->createAdminUser();
        $response = $this->call('GET', '/categories');
        $this->assertEquals(200, $response->status());
    }

    /**
     * Checks for collection returned validity
     */
    public function testCategoryIndexContents()
    {
        $this->createAdminUser();
        $this->call('GET', '/categories');
        $data = $this->response->getOriginalContent()->getData();
        $this->assertArrayHasKey('categories', $data);
        $this->assertArrayHasKey('_items', $data['categories']);

        $index = 0;
        foreach ($data['categories']['_items'] as $cat) {
            $this->assertEquals($cat['name'], $this->categories[$index++]->name);
        }
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCategoryIndeNotxAuthenticated()
    {
        $this->call('GET', '/categories');
        $this->assertRedirectedToRoute('login');
    }

    public function tearDown()
    {
        parent::tearDown();
//
//        dd($this->categories);
//        $this->categories[0]->filter(function($category){
//            $category->delete();
//        });
    }
}
