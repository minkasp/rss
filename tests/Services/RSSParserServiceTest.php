<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RSSParserServiceTest extends TestCase
{
    /** @var \Illuminate\Foundation\Application|mixed|null  */
    protected $parser = null;

    /**
     * RSSParserServiceTest constructor.
     */
    public function setUp()
    {
        $this->parser = app(\App\Services\RSSParserService::class);
        parent::setUp();
    }

    /***
     * @return void
     */
    public function testParsingWithNoSources()
    {
        $data = $this->parser->parse();
        $this->assertCount(0, $data);
    }

    /**
     * @dataProvider sourceProvider
     */
    public function testSetSourceNotArray($source)
    {
         $data = $this->parser->setSources($source)->parse();
         $this->assertTrue(!empty($data));
    }

    /**
     * @dataProvider sourceProvider
     */
    public function testSetSourceArray($source)
    {
        $data = $this->parser->setSources([$source])->parse();
        $this->assertTrue(!empty($data));
    }

    /**
     * @dataProvider sourceProvider
     */
    public function testFeedData($source)
    {
        $data = $this->getData($source);
        $pos = random_int(0, count($data)-1);

        $this->assertTrue(!empty($data));
        $this->assertTrue(!empty($data[$pos]->getItems()));

        $feeds =  $data[$pos]->getItems();

        $feedPos = random_int(0, count($feeds)-1);
        $feed = $feeds[$feedPos];

        $this->assertNotNull($feed['title']);
        $this->assertNotNull($feed['link']);
        $this->assertNotNull($feed['description']);
        $this->assertNotNull($feed['publish_date']);
    }

    /**
     * @dataProvider sourceProvider
     * @param $source
     */
    public function testFeedGetters($source)
    {
        $feed = $this->getFeed($source);

        $this->assertNotNull($feed->getTitle());
        $this->assertNotNull($feed->getLink());
        $this->assertTrue(!empty($feed->getItems()));
    }

    /**
     * @param $source
     * @return mixed
     */
    private function getData($source)
    {
        return $this->parser->setSources([$source])->parse();
    }

    /**
     * @param $source
     * @return mixed
     */
    public function getFeed($source)
    {
        $data = $this->getData($source);
        $pos = random_int(0, count($data)-1);

        return $data[$pos];
    }

    /**
     * Source data provider
     * @return array
     */
    public function sourceProvider()
    {
        return [
            ['http://www.15min.lt/rss'],
            ['http://www.lrt.lt/naujienos?rss'],
        ];
    }
}
